#
# Run an OpenSSH server
#
# Build:
# docker build -t "cyrille37/openssh-server" -f openssh.dockerfile .
# Run:
# docker run -p 22:22 --name "sshd" --rm cyrille37/openssh-server
# Stop:
# docker stop sshd
#

FROM debian:12-slim
#FROM debian:12

LABEL MAINTAINER Cyrille37

# Add "contrib" component to debian sources
#RUN sed -i 's/^Components\: main$/Components\: main contrib/' /etc/apt/sources.list.d/debian.sources

# Don't forget to call "apt update" before software installation
RUN apt -y update && apt -y install openssh-server sudo locales \
    && rm -rf /var/lib/apt/lists/*

# set locale to fr_FR to allow french accented characters
RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
ENV LANG fr_FR.utf8

# create user "milou" with password "secret" and permits `sudo` without password
RUN useradd -m -s /bin/bash -G sudo -u 1000 milou
RUN usermod -aG sudo milou
RUN echo 'milou:secret' | chpasswd
RUN echo "milou ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

RUN mkdir /home/milou/.ssh
COPY id_rsa.nopasswd /home/milou/.ssh/id_rsa
COPY id_rsa.pub /home/milou/.ssh/id_rsa.pub
RUN chmod 600 /home/milou/.ssh/id_rsa
RUN cat /home/milou/.ssh/id_rsa.pub > /home/milou/.ssh/authorized_keys
RUN chmod 600 /home/milou/.ssh/authorized_keys
RUN chown -R milou:milou /home/milou/.ssh

# Mandatory to avoid "Missing privilege separation directory: /run/sshd"
RUN service ssh start

EXPOSE 22

CMD ["/usr/sbin/sshd","-D"]
