# Docker openssh hosts

Easily launch two hosts waiting for you on SSH port 22.

Default run with [docker compose](#docker-compose) without any configuration.

## Prerequisites

Tested with
- docker 24
- docker-compose 1.29

To avoid "It is required that your private key files are NOT accessible by others."
```
chmod 600 docker/id_rsa.nopasswd
```

## Docker

### docker compose

```
# launch containers as service
docker-compose build
docker-compose up -d
# connect to container
ssh -i docker/id_rsa.nopasswd milou@172.28.1.5
ssh -i docker/id_rsa.nopasswd milou@172.28.1.6
```

To change some defaults use the `.env.example` file to fill a `.env` one.

### docker solo

Build image

```
docker build -t "cyrille37/openssh-server" -f docker/openssh.dockerfile ./docker/
```

Run container

```
docker run -p 22:22 --name "sshd" --rm cyrille37/openssh-server
```

Ssh into the container

```
ssh -i docker/id_rsa.nopasswd milou@localhost
```
